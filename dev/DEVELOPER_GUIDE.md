# Developer Guide

## Documentation

For generating documentation, we use [pydoc-markdown](https://pydoc-markdown.readthedocs.io/en/latest/) and [mkdocs-material](https://squidfunk.github.io/mkdocs-material/).

### Local development

Install the requirements in root
```bash
pip install -r requirements.txt
```

Build the package locally
```bash
pip install .
```

Import deeploy in a new Python or Notebook file (or use the `client.ipynb`)
```python
import deeploy
```


### Testing

Run the following command in root
```bash
python3 -m pytest .
```


### Generate

1. Generate the markdown from python code

    ```bash
    pydoc-markdown --render-toc -v
    ```

2. Generate HTML from the markdown

    ```bash
    mkdocs build -f docs/mkdocs.yml
    ```