from enum import Enum


class TransformerType(Enum):
    """Class that contains transformer types"""

    NO_TRANSFORMER = 0
    CUSTOM = 1
