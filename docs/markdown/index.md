<p align="center">
    <img align="center" src="./img/Logo%20(original).png" width="250px" />
</p>

<div align="center">
    <b>Deeploy - Making Machine Learning Explainable</b>
    <br>
    <img align="center" src="https://img.shields.io/pypi/l/deeploy.svg?color=blue" />
    <a href="https://pypi.org/project/deeploy/"><img align="center" src="https://img.shields.io/pypi/v/deeploy.svg" /></a>
    <a href="https://gitlab.com/deeploy-ml/deeploy-python-client/pipelines"><img align="center" src="https://gitlab.com/deeploy-ml/deeploy-python-client/badges/master/pipeline.svg" /></a>
    <br>
</div>
<br>

The Deeploy Python Client for the Deeploy Core product is build to simplify communication with the Deeploy API from your (local) python environment.

- [API Reference](./api-reference.md)
- [Python Client Docs](https://docs.deeploy.ml/python-client/introduction)
