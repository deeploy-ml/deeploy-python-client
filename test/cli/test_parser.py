from deeploy.cli.parsers.parser_model import parse_args_model

MODEL_DIR = "/tmp/custom_model"  # noqa: S108
MODEL_NAME = "default"
NTHREAD = "2"


def test_basic_args_model():
    args = ["--model_dir", MODEL_DIR, "--model_name", MODEL_NAME, "--nthread", NTHREAD]
    parser = parse_args_model(args)
    assert parser.model_dir == MODEL_DIR
    assert parser.model_name == MODEL_NAME
    assert parser.nthread == NTHREAD
