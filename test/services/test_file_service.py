from unittest.mock import mock_open, patch

import pytest
from pydantic import ValidationError

from deeploy.models import (
    CreateExplainerReference,
    CreateModelReference,
    CreateTransformerReference,
)
from deeploy.services import FileService


@pytest.fixture(scope="session")
def file_service():
    return FileService()



@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_model_reference_json_docker(open, mkdir, file_service):
    reference_input = {
        "docker": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
        }
    }
    expected_output = {
        "reference": {
            "docker": {
                "image": "image",
                "uri": "uri",
                "port": 1234,
            }
        }
    }

    output = file_service.generate_reference_json("", CreateModelReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_model_reference_json_blob(open, mkdir, file_service):
    reference_input = {"blob": {"url": "url", "region": "region"}}
    expected_output = {"reference": {"blob": {"url": "url", "region": "region"}}}
    output = file_service.generate_reference_json("", CreateModelReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_model_reference_json_mlflow(open, mkdir, file_service):
    reference_input = {
        "mlflow": {
            "model": "model",
            "stage": "stage",
            "version": "version",
            "blob": {"region": "region"},
        }
    }
    expected_output = {
        "reference": {
            "mlflow": {
                "model": "model",
                "stage": "stage",
                "version": "version",
                "blob": {"region": "region"},
            }
        }
    }
    output = file_service.generate_reference_json("", CreateModelReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_model_reference_json_azure_ml(open, mkdir, file_service):
    reference_input = {
        "azure_ml": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
            "readiness_path": "path",
            "liveness_path": "path",
            "model": "model",
            "version": "version",
        }
    }
    expected_output = {
        "reference": {
            "azureML": {
                "image": "image",
                "uri": "uri",
                "port": 1234,
                "readinessPath": "path",
                "livenessPath": "path",
                "model": "model",
                "version": "version",
            }
        }
    }
    output = file_service.generate_reference_json("", CreateModelReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_model_reference_json_with_wrong_input(open, mkdir, file_service):
    wrong_reference_input = {
        "docker": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
        },
        "azure_ml": {"image": "image"},
    }

    with pytest.raises(ValidationError):
        file_service.generate_reference_json("", CreateModelReference(**wrong_reference_input))


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_explainer_reference_json_docker(open, mkdir, file_service):
    reference_input = {
        "docker": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
        }
    }
    expected_output = {
        "reference": {
            "docker": {
                "image": "image",
                "uri": "uri",
                "port": 1234,
            }
        }
    }

    output = file_service.generate_reference_json("", CreateExplainerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_explainer_reference_json_blob(open, mkdir, file_service):
    reference_input = {"blob": {"url": "url", "region": "region"}}
    expected_output = {"reference": {"blob": {"url": "url", "region": "region"}}}
    output = file_service.generate_reference_json("", CreateExplainerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_explainer_reference_json_mlflow(open, mkdir, file_service):
    reference_input = {
        "mlflow": {
            "model": "model",
            "stage": "stage",
            "version": "version",
            "blob": {"region": "region"},
        }
    }
    expected_output = {
        "reference": {
            "mlflow": {
                "model": "model",
                "stage": "stage",
                "version": "version",
                "blob": {"region": "region"},
            }
        }
    }
    output = file_service.generate_reference_json("", CreateExplainerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_explainer_reference_json_azure_ml(open, mkdir, file_service):
    reference_input = {
        "azure_ml": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
            "readiness_path": "path",
            "liveness_path": "path",
            "model": "model",
            "version": "version",
        }
    }
    expected_output = {
        "reference": {
            "azureML": {
                "image": "image",
                "uri": "uri",
                "port": 1234,
                "readinessPath": "path",
                "livenessPath": "path",
                "model": "model",
                "version": "version",
            }
        }
    }
    output = file_service.generate_reference_json("", CreateExplainerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_explainer_reference_json_with_wrong_input(open, mkdir, file_service):
    wrong_reference_input = {
        "docker": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
        },
        "mlflow": {"stage": "stage"},
    }

    with pytest.raises(ValidationError):
        file_service.generate_reference_json("", CreateExplainerReference(**wrong_reference_input))


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_transformer_reference_json_docker(open, mkdir, file_service):
    reference_input = {
        "docker": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
        }
    }
    expected_output = {
        "reference": {
            "docker": {
                "image": "image",
                "uri": "uri",
                "port": 1234,
            }
        }
    }

    output = file_service.generate_reference_json("", CreateTransformerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_transformer_reference_json_blob(open, mkdir, file_service):
    reference_input = {"blob": {"url": "url", "region": "region"}}
    expected_output = {"reference": {"blob": {"url": "url", "region": "region"}}}
    output = file_service.generate_reference_json("", CreateTransformerReference(**reference_input))
    assert output == expected_output


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_transformer_reference_json_with_azure_ml_input(open, mkdir, file_service):
    wrong_reference_input = {
        "azure_ml": {
            "image": "image",
            "uri": "uri",
            "port": 1234,
            "readiness_path": "path",
            "liveness_path": "path",
            "model": "model",
            "version": "version",
        }
    }

    with pytest.raises(ValueError):
        file_service.generate_reference_json(
            "", CreateTransformerReference(**wrong_reference_input)
        )


@patch("builtins.open", new_callable=mock_open, read_data="data")
@patch("os.mkdir")
def test_generate_transformer_reference_json_with_mlflow_input(open, mkdir, file_service):
    wrong_reference_input = {
        "mlflow": {
            "model": "model",
            "stage": "stage",
            "version": "version",
            "blob": {"region": "region"},
        }
    }

    with pytest.raises(ValueError):
        file_service.generate_reference_json(
            "", CreateTransformerReference(**wrong_reference_input)
        )
